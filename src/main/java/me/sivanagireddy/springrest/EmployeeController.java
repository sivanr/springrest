
package me.sivanagireddy.springrest;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author siva
 */
@RestController
public class EmployeeController {
    
    @RequestMapping(value = "/allemp", method = RequestMethod.GET)
    public EmpList getAllEmployees(){
        List<Employee> empList = new ArrayList<>();
        empList.add(new Employee("1001", "Olivia Reddy",30, "Melbourne", true));
        empList.add(new Employee("1002", "John Smith",31, "New York", false));
        return new EmpList(empList);
    }
    
}
