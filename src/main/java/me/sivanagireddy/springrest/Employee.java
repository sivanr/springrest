package me.sivanagireddy.springrest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author siva
 */
@XmlRootElement(name = "employee")
@XmlType(propOrder = {"id", "name", "age", "city", "active"})
public class Employee {
    
    private String id;
    private String name;
    private Integer age;
    private String city;
    private Boolean active;

    public Employee() {
    }

    public Employee(String id, String name, Integer age, String city, Boolean active) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.city = city;
        this.active = active;
    }
    
    @XmlElement
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @XmlElement
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @XmlElement
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
