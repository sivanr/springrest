
package me.sivanagireddy.springrest;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author siva
 */
@XmlRootElement(name = "employees")
public class EmpList {
    private List<Employee> empList;

    public EmpList() {
    }

    public EmpList(List<Employee> empList) {
        this.empList = empList;
    }

    @XmlElement(name = "employee")
    public List<Employee> getEmpList() {
        return empList;
    }

    public void setEmpList(List<Employee> empList) {
        this.empList = empList;
    }
    
}
